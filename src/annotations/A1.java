package annotations;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class A1 {

	@BeforeSuite
	public void beforeSuite(){
		System.out.println("In before suite -- a1");
	}
	
	@BeforeTest
	public void beforeTest(){
		System.out.println("In before test ");
	}
	
	@BeforeClass
	public void beforeclass(){
		System.out.println("In before class -- a1");
	}
	
	@BeforeMethod
	public void beforeMethod(){
		System.out.println("In before Method ");
	}
	
	@AfterMethod
	public void afterMethod(){
		System.out.println("In after Method ");
	}
	
	@Test(groups="smoke")
	public void test1(){
		System.out.println("In test 1");
	}
	
	@Test(groups="regression")
	public void test2(){
		System.out.println("In test 2");
	}
	

	@Test(groups={"smoke","regression"})
	public void test3(){
		System.out.println("In test 3");
	}
}
